class CreateOpinions < ActiveRecord::Migration[5.2]
  def change
    create_table :opinions do |t|
      t.string :description
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
