# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

restaurant = Restaurant.create(name: 'Prueba')
restaurant.save

opinion_one = Opinion.create(description: 'Opcion 1', restaurant_id: restaurant.id)
opinion_one.save

opinion_two = Opinion.create(description: 'Opcion 2', restaurant_id: restaurant.id)
opinion_two.save