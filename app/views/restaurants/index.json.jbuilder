json.array! @restaurants do |restaurant|
  json.id restaurant.id
  json.name restaurant.name

  json.opinions do
    json.partial! 'opinions/opinion', opinions: restaurant.opinions
  end
end