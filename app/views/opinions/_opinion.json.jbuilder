json.array! opinions do |opinion|
  json.id opinion.id
  json.description opinion.description
  json.restaurant_id opinion.restaurant_id
end