class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:update]

  # GET /restaurants
  def index
    @restaurants = Restaurant.all
  end

  # POST /restaurantsCommits on Sep 24, 2018
  def create
    restaurants = repair_nested_params(params["restaurants"])
    @restaurants_created = []

    restaurants.each do |restaurant|
      if restaurant.second["id"] == -1
        @restaurants_created << Restaurant.create(restaurant_params(restaurant.second))
      else
        @restaurant = Restaurant.find(restaurant.second["id"])
        if @restaurant
          @restaurant.update(restaurant_params(restaurant.second))
          @restaurants_created << @restaurant
        end
      end
    end

    render json: @restaurants_created, status: :created, location: @restaurant
  end

  # PATCH/PUT /restaurants/1
  def update
    if @restaurant.update(restaurant_params)
      render json: @restaurant
    else
      render json: @restaurant.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def restaurant_params(restaurant_params)
      restaurant_params.permit(
        :name,
        opinions_attributes:  [
          :id,
          :description,
          :restaurant_id
        ])
    end

    def repair_nested_params(obj)
      obj.each do |key, value|
        if value.is_a? ActionController::Parameters
          # If any non-integer keys
          if value.keys.find {|k, _| k =~ /\D/ }
            repair_nested_params(value)
          else
            obj[key] = value.values
            value.values.each {|h| repair_nested_params(h) }
          end
        end
      end
    end
end
