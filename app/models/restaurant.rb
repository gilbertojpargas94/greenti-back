class Restaurant < ApplicationRecord
  has_many :opinions

  accepts_nested_attributes_for :opinions, allow_destroy: true

  validates :name, presence: true

  validates_associated :opinions, on: [:create, :update] #validate the opinions are fine, the opinions alone as an object have their own validations but when they are created in another model ignores their validations
end
