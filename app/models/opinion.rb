class Opinion < ApplicationRecord
  belongs_to :restaurant

  validates :description, presence: true
end
